package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.*;

class Bank implements BankOperation {

    private final List<Account> accountData = new ArrayList<>();

    public static List<Account> getAccountsList(){

        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(1, 1000));
        accounts.add(new Account(2, 2000));
        accounts.add(new Account(3, 3000));
        accounts.add(new Account(4, 4000));
        accounts.add(new Account(5, 5000));
        accounts.add(new Account(6, 6000));
        accounts.add(new Account(7, 7000));
        accounts.add(new Account(8, 8000));
        accounts.add(new Account(9, 9000));
        accounts.add(new Account(10, 10000));

        return accounts;
    }

    private static int accountNumber = 0;
    public void addSampleAccounts(List<Account> accounts){
        accountData.addAll(accounts);
        accountNumber = accounts.size();
    }

    public int createAccount() {
        accountData.add(new Account(++accountNumber, 0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {

        int balance = ACCOUNT_NOT_EXIST;

        Account account = findAccount(accountNumber).orElse( new Account(ACCOUNT_NOT_EXIST,0));
        if (account.getAccountNumber() != ACCOUNT_NOT_EXIST) {
            balance = account.getBalance();
            accountData.remove(account);
            Bank.accountNumber--;
        }
        return balance;
    }

    public boolean deposit(int accountNumber, int amount) {

        Account account = findAccount(accountNumber).orElse( new Account(ACCOUNT_NOT_EXIST,0));

        if(account.getAccountNumber() != ACCOUNT_NOT_EXIST && amount > 0){
            account.setBalance(account.getBalance() + amount);
            return true;
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        if(amount <= 0){ return false; }

        Account account = findAccount(accountNumber).orElse( new Account(ACCOUNT_NOT_EXIST,0));

        if(account.getAccountNumber() != ACCOUNT_NOT_EXIST && account.getBalance() >= amount){
            account.setBalance(account.getBalance() - amount);
            return true;
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount){
        if(fromAccount == toAccount || amount <= 0){ return false; }

        Account fromTmp = findAccount(fromAccount).orElse( new Account(ACCOUNT_NOT_EXIST,0));
        Account toTmp = findAccount(toAccount).orElse( new Account(ACCOUNT_NOT_EXIST,0));

        if (fromTmp.getAccountNumber() != ACCOUNT_NOT_EXIST) {
            if (toTmp.getAccountNumber() != ACCOUNT_NOT_EXIST && fromTmp.getBalance() >= amount) {

                fromTmp.setBalance(fromTmp.getBalance() - amount);
                toTmp.setBalance(toTmp.getBalance() + amount);
                return true;
            }
        }

        return false;
    }
    public int accountBalance(int accountNumber) {
        Account account = findAccount(accountNumber).orElse( new Account(ACCOUNT_NOT_EXIST,0));

        if(account.getAccountNumber() != ACCOUNT_NOT_EXIST){
            int balance= account.getBalance();
            return balance;
        }
        return ACCOUNT_NOT_EXIST;
    }

    public int sumAccountsBalance() {
        int sum = accountData.stream().mapToInt(Account::getBalance).sum();
        return sum;
    }

    private Optional<Account> findAccount(int accountNumber) {
        for (Account account : accountData) {
            if (account.getAccountNumber() == accountNumber) {
                return Optional.of(account);
            }
        }
        return Optional.empty();
    }
}
