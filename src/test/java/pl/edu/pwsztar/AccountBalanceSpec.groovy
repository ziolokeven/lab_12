package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification{

    @Unroll
    def "should show accountBalance #accountBalance for account #account"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = bank.accountsList
        bank.addSampleAccounts(accounts)
        when: "get account balance"
        def result = bank.accountBalance(account)
        then: "check balance"
        result == accountBalance
        where:
        account  |  accountBalance
        1        |  1000
        2        |  2000
        3        |  3000
        4        |  4000
        5        |  5000
        6        |  6000
        7        |  7000
        8        |  8000
        9        |  9000
        10       | 10000
    }
}
