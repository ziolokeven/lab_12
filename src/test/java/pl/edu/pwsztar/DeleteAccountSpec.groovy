package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec  extends Specification {

    @Unroll
    def "should delete account #accountNumber show accountBalance #accountBallance"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = bank.accountsList
        bank.addSampleAccounts(accounts)
        when: "deleted account"
        def result = bank.deleteAccount(accountNumber)
        then: "check deleted account"
        result == accountBalance

        where:
        accountNumber   |   accountBalance
        1               |  1000
        2               |  2000
        3               |  3000
        4               |  4000
        5               |  5000
        6               |  6000
        7               |  7000
        8               |  8000
        9               |  9000
        10              | 10000
    }
}