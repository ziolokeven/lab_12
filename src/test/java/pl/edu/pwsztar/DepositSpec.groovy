package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification{

    @Unroll
    def "should deposit money from #Accountnumber"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = bank.accountsList
        bank.addSampleAccounts(accounts)
        when: "deposit cash"
        def result = bank.deposit(accountNumber, cash)
        then: "check if deposit is successful"
        result
        where:
        accountNumber   |   cash
        1               |   100
        2               |   200
        3               |   300
        4               |   400
        5               |   500
        6               |   600
        7               |   700
        8               |   800
        9               |   900
        10              |  1000

    }
}

