package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountBalance extends Specification{

    @Unroll
    def "should check balance account and return this balance"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = bank.accountsList
        bank.addSampleAccounts(accounts)
        when: "sum balance from all account"
        def result = bank.sumAccountsBalance()
        then: "check sum"
        result == 55000
    }
}