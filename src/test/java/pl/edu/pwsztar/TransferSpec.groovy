package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification{

    @Unroll
    def"should transfer from #senderAccount to #recipientAccount account cash #cash"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = bank.accountsList
        bank.addSampleAccounts(accounts)
        when: "transfer money"
        def result = bank.transfer(sourceAccount, destinyAccount, cash)
        then: "check if transfer is successful"
        result
        where:
        sourceAccount   |   destinyAccount  |   cash
        1               |         10        |   333
        2               |         9         |   444
        3               |         8         |   555
        4               |         7         |   666
        5               |         6         |   777
        6               |         5         |   888
        7               |         4         |   999
        8               |         4         |   123
        9               |         2         |   321
        10              |         1         |   2000

    }
}