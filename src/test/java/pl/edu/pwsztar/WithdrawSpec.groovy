package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification{

    @Unroll
    def "should withdraw #cash of #accountBalance from Account #accountNumber"(){
        given: "initial data"
        def accounts = Bank.accountsList
        def bank = new Bank()
        bank.addSampleAccounts(accounts)

        when: "withdraw cash"
        def result = bank.withdraw(accountNumber, cashToWithdraw)
        then: "check if it draw was successful"
        result

        where:
        accountNumber | cash | cashToWithdraw
        1             |  1000 |      900
        2             |  2000 |      1900
        3             |  3000 |      2800
        4             |  4000 |      3300
        5             |  5000 |      4500
        6             |  6000 |      5500
        7             |  7000 |      6700
        8             |  8000 |      7900
        9             |  9000 |      9000
        10            | 10000 |      10000
    }
}
